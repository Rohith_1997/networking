import socket ,threading
class ClientThread(threading.Thread):
	def _init_(self,c_address,c_socket):
		threading.Thread._init_(self)
		self.client_soc = c_socket
		print("A new connection got added:",c_address)
	def run(self):
		msg = ''
		while True :
			data = self.client_soc.recv(1024)
			msg = data.decode()
			self.client_soc.send(bytes(msg,'UTF-8'))


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(("127.0.0.1",8080))
while True :
	server.listen(1)
	c_socket,c_address = server.accept()
	newThead = ClientThread(c_socket,c_address)
	newThead.start()
