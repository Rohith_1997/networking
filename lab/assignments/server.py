 
import socket, threading
class ClientThread(threading.Thread):
    def __init__(self,c_address,c_socket):
        threading.Thread.__init__(self)
        self.csocket = c_socket
        print ("New connection added: ", c_address)
    def run(self):
        msg = ''
        while True:
            data = self.csocket.recv(2048)
            msg = data.decode()
            print ("from client", msg)
            self.csocket.send(bytes(msg,'UTF-8'))

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind(("127.0.0.1", 8080))
while True:
    server.listen(1)
    c_sock, c_address = server.accept()
    newthread = ClientThread(c_address, c_sock)
    newthread.start()
